import time
from pyrogram import Client, filters, errors
from pyrogram.types import InputPhoneContact
import tgcrypto
import uuid

"""
body_1 = {
    "name_body":"content_for_import",
    "session_string": session_string,
    "api_id": api_id,
    "api_hash": api_hash,
    "numbers": numbers
}

body_2 = {
    "name_body":"content_for_add",
    "session_string": session_string,
    "api_id": api_id,
    "api_hash": api_hash,
    "numbers": numbers,
    "link_chat":link,
    "time_sleep":time
}

"""


def telegram_import_number_to_contacts(body):
    new_uid = uuid.uuid1()
    result_list = str(new_uid).split("-")
    app = Client(name=result_list[0], session_string=body['session_string'], api_id=body['api_id'],
                 api_hash=body['api_hash'])
    app.start()

    success_number = []
    failed = []

    for number in body['numbers']:

        try:

            new_uid = uuid.uuid1()
            result_list = str(new_uid).split("-")
            x = app.import_contacts([InputPhoneContact(number, result_list[0])])

            if x.imported:

                data = {
                    'number':number,
                    'id':x.imported[0].user_id
                }
                success_number.append(data)

            elif not x.imported:

                failed.append(number)

        except errors.exceptions.PeerFlood:

            return 401, """ {"stat": False, "message": "This account is subject to a time limit by Telegram."}"""

        except errors.exceptions.BadRequest:

            if number not in failed:
                failed.append(number)


        except errors.exceptions.FloodWait:

            return 400, """ {"stat": False,"message": "This device is being used by the Cable، which is used by other users."}"""

    app.stop()

    return 200, {"success_number": success_number, "failed": failed}


def telegram_add_to_chat(body):
    new_uid = uuid.uuid1()
    result_list = str(new_uid).split("-")

    app = Client(name=result_list[0], session_string=body['session_string'], api_id=body['api_id'],
                 api_hash=body['api_hash'])
    app.start()

    gp = app.join_chat(body['link_chat'])

    success_number = []
    failed = []

    for number in body['numbers']:

        try:

            app.add_chat_members(gp.id, number['id'])
            success_number.append(number['number'])

        except errors.exceptions.PeerFlood:

            return 402, """{"stat":False,"message":"This account is subject to a time limit by Telegram"}"""

        except errors.exceptions.FloodWait:

            return 403, """{"stat": False,"message": "This device is being used by the Cable، which is used by other users."}"""

        except errors.exceptions.BadRequest:

            failed.append(number)

        if body['time_sleep'] == None:
            time.sleep(5)

        else:

            time.sleep(body['time_sleep'])

    app.stop()

    return 200, {"success_number": success_number, "failed": failed}


